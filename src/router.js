import Vue from "vue";
import Router from "vue-router";
import Login from "./views/Login";
import Albums from "./views/Albums";
import Artists from "./views/Artists";
import Radios from "./views/Radios";
import Boxes from "./views/Boxes";
import Search from "./views/Search";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: "/login"
    },
    {
      path: "/login",
      component: Login
    },
    {
      path: "/albums",
      component: Albums
    },
    {
      path: "/albums/:id",
      component: Albums
    },
    {
      path: "/artists",
      component: Artists
    },
    {
      path: "/artists/:id",
      component: Artists
    },
    {
      path: "/radios",
      component: Radios
    },
    {
      path: "/boxes",
      component: Boxes
    },
    {
      path: "/boxes/:id",
      component: Boxes
    },
    {
      path: "/search",
      component: Search
    }
  ]
});
