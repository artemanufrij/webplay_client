import Vue from "vue";
import Vuex from "vuex";
import EventBus from "./bus";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    artists: [],
    selectedArtist: {},
    albums: [],
    selectedAlbum: {},
    boxes: [],
    selectedBox: {},
    radios: [],
    selectedRadio: {},
    selectedVideo: {},
    selectedTrack: { title: "", parent: { title: "" } },
    player: {
      isPlaying: false,
      repeat: "none",
      shuffle: false,
      percent: 0
    },
    serverStatus: { scanning_music: false, scanning_video: false },
    searchFilter: "",
    me: { history: [], roles: [] },
    artistsMounted: false,
    albumsMounted: false,
    boxesMounted: false,
    radiosMounted: false,
    server: ""
  },
  getters: {
    amImod: state => {
      return (
        state.me.roles &&
        (state.me.roles.includes("admin") ||
          state.me.roles.includes("moderator"))
      );
    },
    amIadmin: state => {
      return state.me.roles && state.me.roles.includes("admin");
    },
    headers: state => {
      return {
        headers: { authorization: state.me.token }
      };
    }
  },
  mutations: {
    switchPlayerShuffleMode(state) {
      state.player.shuffle = !state.player.shuffle;
      EventBus.$emit("player_settings_changed");

      if (!state.selectedTrack._id) {
        return;
      }

      var shuffle = state.player.shuffle;
      var parent = state.selectedTrack.parent;

      if (shuffle && !parent.shuffleHistory) {
        parent.shuffleHistory = [];
        if (state.player.isPlaying) {
          parent.shuffleHistory.push(
            parent.tracks.indexOf(state.selectedTrack)
          );
        }
      } else if (!shuffle && parent.shuffleHistory) {
        parent.shuffleHistory = null;
      }
    },
    switchPlayerRepeatMode(state) {
      switch (state.player.repeat) {
        case "all":
          state.player.repeat = "one";
          break;
        case "one":
          state.player.repeat = "none";
          break;
        default:
          state.player.repeat = "all";
          break;
      }
      EventBus.$emit("player_settings_changed");
    },
    resetSelectedTrack(state) {
      if (!state.selectedTrack._id) {
        return;
      }
      if (state.selectedTrack.parent) {
        state.selectedTrack.parent.shuffleHistory = null;
      }
      state.selectedTrack = { title: "", parent: { title: "" } };
    },
    resetSelectedArtist(state) {
      state.selectedArtist = {};
    },
    resetSelectedAlbum(state) {
      state.selectedAlbum = {};
    },
    resetSelectedBox(state) {
      state.selectedBox = {};
    },
    resetSelectedRadio(state) {
      if (!state.selectedRadio._id) {
        return;
      }
      state.selectedRadio = {};
    },
    selectAlbum(state, id) {
      if (state.albumsMounted) {
        state.selectedAlbum = state.albums.find(f => f._id == id);
        state.selectedAlbum.selected();
      }
    },
    selectArtist(state, id) {
      if (state.artistsMounted) {
        state.selectedArtist = state.artists.find(f => f._id == id);
        state.selectedArtist.selected();
      }
    },
    selectBox(state, id) {
      if (state.boxesMounted) {
        state.selectedBox = state.boxes.find(f => f._id == id);
        state.selectedBox.selected();
      }
    }
  }
});
